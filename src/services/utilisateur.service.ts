import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { RequestOptions, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Utilisateur } from '../models/utilisateur';

@Injectable()
export class UtilisateurService {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private apiUrl = 'http://vps287496.ovh.net/api-generic/resources/testLSU/';

  constructor(private http: Http) { }

  getInactiveUsers(): Promise<Utilisateur[]> {
    return this.http.get(this.apiUrl + "getInactiveUsers")
      .toPromise()
      .then(response => response.json() as Utilisateur[])
      .catch(this.handleError);
  }

  getInactiveUser(): Promise<Utilisateur>  {
    return this.http.get(this.apiUrl + "getInactiveUser")
    .toPromise()
    .then(response => response.json() as Utilisateur)
    .catch(this.handleError);
  }

    getUtilisateurs(): Promise<Utilisateur[]> {
    return this.http.get(this.apiUrl + "")
      .toPromise()
      .then(response => response.json() as Utilisateur)
      .catch(this.handleError);
  }

  postUtilisateur(utilisateur:Utilisateur): Promise<Utilisateur> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.apiUrl, utilisateur, options).toPromise()
           .then(this.extractData)
           .catch(this.handleError);
  } 

  extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}