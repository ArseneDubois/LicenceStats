import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { NbUtilisateurs } from '../models/nbUtilisateurs';

@Injectable()
export class NbUtilisateursService {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private apiUrl = 'http://localhost:8090/';

  constructor(private http: Http) { }

  getNbUtilisateursJira(): Promise<NbUtilisateurs> {
    return this.http.get(this.apiUrl + "getUserCtJira")
      .toPromise()
      .then(response => response.json() as NbUtilisateurs)
      .catch(this.handleError);
  }

  getNbUtilisateursStash(): Promise<NbUtilisateurs> {
    return this.http.get(this.apiUrl + "getUserCtStash")
      .toPromise()
      .then(response => response.json() as NbUtilisateurs)
      .catch(this.handleError);
  }

  getNbUtilisateursConfluence(): Promise<NbUtilisateurs> {
    return this.http.get(this.apiUrl + "getUserCtConfluence")
      .toPromise()
      .then(response => response.json() as NbUtilisateurs)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}