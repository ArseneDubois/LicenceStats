import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Application } from '../models/application';

@Injectable()
export class ApplicationService {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private apiUrl = 'http://vps287496.ovh.net/api-generic/resources/testLSA/';

  constructor(private http: Http) { }


    getApplications(): Promise<Application[]> {
    return this.http.get(this.apiUrl + "")
      .toPromise()
      .then(response => response.json() as Application)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}