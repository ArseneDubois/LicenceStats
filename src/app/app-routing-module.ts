import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListeUtilisateursComponent } from '../components/listeUtilisateurs/listeUtilisateurs.component';
import { AjouterUtilisateurComponent } from '../components/ajouterUtilisateur/ajouterUtilisateur.component';
import { ListeApplicationsComponent } from '../components/listeApplications/listeApplications.component';

const routes: Routes = [
	{ path: '', redirectTo: '/accueil', pathMatch: 'full'},
	{ path: 'accueil', component: ListeUtilisateursComponent },
	{ path: 'ajouterUtilisateur', component: AjouterUtilisateurComponent },
	{ path: 'listeApplications', component: ListeApplicationsComponent }

];

@NgModule({
	imports: [ RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule {}