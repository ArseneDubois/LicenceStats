import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/primeng';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class AppComponent{
    title = 'LicenceStats';

    constructor(private router:Router) {

    }

    private goTo(routePath:String): void {
        this.router.navigate([routePath]);
    }

}