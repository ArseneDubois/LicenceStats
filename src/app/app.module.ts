import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';

import { MenuModule, MenuItem, SlideMenuModule, ButtonModule, ChartModule, TieredMenuModule, TooltipModule, DataTableModule, SharedModule, Message, GrowlModule } from 'primeng/primeng';

import { AppRoutingModule } from './app-routing-module';
import { AppComponent } from './app.component';


import {InputTextModule} from 'primeng/inputtext';
import {ListboxModule} from 'primeng/listbox';

import { ListeUtilisateursComponent } from '../components/listeUtilisateurs/listeUtilisateurs.component';
import { ListeApplicationsComponent } from '../components/listeApplications/listeApplications.component';
import { AjouterUtilisateurComponent } from '../components/ajouterUtilisateur/ajouterUtilisateur.component';
import { PieChartComponent } from '../components/pieChart/pieChart.component';
import { DoughnutChartComponent } from '../components/doughnutChart/doughnutChart.component';
import { BarChartComponent } from '../components/barChart/barChart.component';
import { LineChartComponent } from '../components/lineChart/lineChart.component';
import { MenuChartsComponent } from '../components/menuCharts/menuCharts.component';

import { UtilisateurService } from '../services/utilisateur.service';
import { ApplicationService } from '../services/application.service';
import { NbUtilisateursService } from '../services/nbUtilisateurs.service';


@NgModule({
  declarations: [
    AppComponent,
    ListeUtilisateursComponent,
    ListeApplicationsComponent,
    PieChartComponent,
    DoughnutChartComponent,
    BarChartComponent,
    LineChartComponent,
    MenuChartsComponent,
    AjouterUtilisateurComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MenuModule,
    SlideMenuModule,
    ButtonModule,
    AppRoutingModule,
    ChartModule,
    TieredMenuModule,
    TooltipModule,
    DataTableModule,
    SharedModule,
    GrowlModule,
    InputTextModule,
    ListboxModule
  ],
  providers: [
    UtilisateurService,
    ApplicationService,
    NbUtilisateursService
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }
