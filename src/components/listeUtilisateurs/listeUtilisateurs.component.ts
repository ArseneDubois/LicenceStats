import { Component, OnInit } from '@angular/core';

import { Utilisateur } from '../../models/utilisateur';

import { UtilisateurService } from '../../services/utilisateur.service';

@Component({
  selector: 'my-listeUtilisateurs',
  templateUrl: './listeUtilisateurs.component.html',
  styleUrls: ['./listeUtilisateurs.component.css'],
})

export class ListeUtilisateursComponent implements OnInit {

    utilisateurs: Utilisateur[];

    constructor(private utilisateurService: UtilisateurService) { 
    }

    ngOnInit() {
        //this.utilisateurService.getInactiveUsers().then(inactiveUsers => {this.inactiveUsers = inactiveUsers; console.log(this.inactiveUsers);});
        this.utilisateurService.getUtilisateurs().then(utilisateurs => {this.utilisateurs = utilisateurs; console.log(this.utilisateurs);});
    }

}
