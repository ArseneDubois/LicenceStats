import { Component, OnInit } from '@angular/core';

import { Application } from '../../models/application';

import { ApplicationService } from '../../services/application.service';

@Component({
  selector: 'my-listeApplications',
  templateUrl: './listeApplications.component.html',
  styleUrls: ['./listeApplications.component.css'],
})

export class ListeApplicationsComponent implements OnInit {

    applications: Application[];
    
    

    constructor(private applicationService: ApplicationService) { 
    }

    ngOnInit() {
        this.applicationService.getApplications().then(applications => {this.applications = applications; console.log(this.applications);});
      
    }
}
