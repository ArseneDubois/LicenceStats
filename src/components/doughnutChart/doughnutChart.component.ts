import { Component } from '@angular/core';

@Component({
  selector: 'my-doughnutChart',
  templateUrl: './doughnutChart.component.html',
  styleUrls: ['../../app/app.component.css']
})

export class DoughnutChartComponent {

    data: any;

    constructor() {
        this.data = {
            labels: ['A','B','C'],
            datasets: [
                {
                    data: [300, 50, 100],
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }]    
            };
    }

}