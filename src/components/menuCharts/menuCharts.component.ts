import { Component, OnInit } from '@angular/core';

import { Message } from 'primeng/primeng';

import { NbUtilisateurs } from '../../models/nbUtilisateurs';

import { NbUtilisateursService } from '../../services/nbUtilisateurs.service';

@Component({
    selector: 'my-menuCharts',
    templateUrl: './menuCharts.component.html',
    styleUrls: ['../../app/app.component.css']
  })

export class MenuChartsComponent implements OnInit {
    
        nbUtilisateursJira: NbUtilisateurs;
        nbUtilisateursConfluence: NbUtilisateurs;
        nbUtilisateursStash: NbUtilisateurs;
        msgs: Message[];

        optionsJira: any;
        optionsConfluence: any;
        optionsStash: any;
    
        constructor(private nbUtilisateursService: NbUtilisateursService) {
            this.nbUtilisateursJira = {
                app: 'Jira',
                labels: [],
                datasets: []
            }
            this.nbUtilisateursConfluence = {
                app: 'Confluence',
                labels: [],
                datasets: []
            }
            this.nbUtilisateursStash = {
                app: 'Stash',
                labels: [],
                datasets: []
            }
         }
    
        ngOnInit() {
            this.nbUtilisateursService.getNbUtilisateursJira().then(nbUtilisateursJira => {this.nbUtilisateursJira = nbUtilisateursJira; console.log(this.nbUtilisateursJira);});
            this.nbUtilisateursService.getNbUtilisateursConfluence().then(nbUtilisateursConfluence => {this.nbUtilisateursConfluence = nbUtilisateursConfluence; console.log(this.nbUtilisateursConfluence);});
            this.nbUtilisateursService.getNbUtilisateursStash().then(nbUtilisateursStash => {this.nbUtilisateursStash = nbUtilisateursStash; console.log(this.nbUtilisateursStash);});
        }
    
        selectJira(event) {
            this.msgs = [];
            this.msgs.push({severity: 'info', summary: 'Informations', 'detail': this.nbUtilisateursJira.datasets[event.element._datasetIndex].data[event.element._index].toString() + " utilisateurs " + this.nbUtilisateursJira.datasets[event.element._datasetIndex].label + " au " + this.nbUtilisateursJira.labels[event.element._index]});
        }

        selectConfluence(event) {
            this.msgs = [];
            this.msgs.push({severity: 'info', summary: 'Informations', 'detail': this.nbUtilisateursConfluence.datasets[event.element._datasetIndex].data[event.element._index].toString() + " utilisateurs " + this.nbUtilisateursConfluence.datasets[event.element._datasetIndex].label + " au " + this.nbUtilisateursConfluence.labels[event.element._index]});
        }

        selectStash(event) {
            this.msgs = [];
            this.msgs.push({severity: 'info', summary: 'Informations', 'detail': this.nbUtilisateursStash.datasets[event.element._datasetIndex].data[event.element._index].toString() + " utilisateurs " + this.nbUtilisateursStash.datasets[event.element._datasetIndex].label + " au " + this.nbUtilisateursStash.labels[event.element._index]});
        }

    }