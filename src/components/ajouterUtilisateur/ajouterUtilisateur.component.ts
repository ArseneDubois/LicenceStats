import { Component, OnInit } from '@angular/core';

import { Utilisateur } from '../../models/utilisateur';
import { Application } from '../../models/application';

import { UtilisateurService } from '../../services/utilisateur.service';
import { ApplicationService } from '../../services/application.service';

import {SelectItem} from 'primeng/api';

import {ApplicationUtilisateur} from '../../models/applicationUtilisateur';

@Component({
  selector: 'my-ajouterUtilisateur',
  templateUrl: './ajouterUtilisateur.component.html',
  styleUrls: ['./ajouterUtilisateur.component.css'],
})

export class AjouterUtilisateurComponent implements OnInit {

    utilisateurs: Utilisateur[];
    applications: Application[];
    selectedApplications: string[];

    constructor(private applicationService: ApplicationService, private utilisateurService: UtilisateurService) { 
    }

    ngOnInit() {
        this.applicationService.getApplications().then(applications => {this.applications = applications; console.log(this.applications);});
    }

    handleClick(nom:string, prenom:string, mail:string) {
        let listeApplications = new Array<ApplicationUtilisateur>();
        let uneApplication = new ApplicationUtilisateur("test");
        listeApplications.push(uneApplication);
        let utilisateur = new Utilisateur(nom, prenom, mail, listeApplications);
        this.utilisateurService.postUtilisateur(utilisateur);
    }

}
