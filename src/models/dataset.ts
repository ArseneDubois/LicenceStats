export class Dataset {
    label: string;
    data: number[];
    fill: boolean;
    borderColor: string;
}