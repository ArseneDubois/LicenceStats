export class Application {
    nom: string;
    maximumUtilisateurs: number;
    seuilCritique: number;
    nombreUtilisateurs: number;
}