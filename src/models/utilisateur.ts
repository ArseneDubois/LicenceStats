import {ApplicationUtilisateur} from '../models/applicationUtilisateur';

export class Utilisateur {
  applications: ApplicationUtilisateur[];
  nom: string;
  prenom: string;
  mail: string;

    constructor(nom: string, prenom:string, mail:string, applications: ApplicationUtilisateur[]) {
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.applications = applications;
    }

}