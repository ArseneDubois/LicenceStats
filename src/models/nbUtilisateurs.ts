import { Dataset } from '../models/dataset';

export class NbUtilisateurs {
    app: string;
    labels: string[];
    datasets: Dataset[];
}